lazy val rain = project.in(file("."))
    .settings(
      name := "rain",
      version := "1.0",
      scalaVersion := "2.12.3",
      mainClass in(Compile, run) := Some("rain.Game"),
    )