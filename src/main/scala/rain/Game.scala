/*
 * Copyright 2017, the project authors. All rights reserved.
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE.md file.
 */

package rain

import java.awt.{Canvas, Dimension}
import javax.swing.JFrame

import rain.graphics.Screen
import rain.input.Keyboard
import rain.levels.Level
import rain.utils.FixedStepClock

/** Entry point for the game and primary game [[Canvas]]. */
object Game extends Canvas {
  private val WIDTH = 300
  private val HEIGHT = WIDTH / 16 * 9
  private val SCALE = 3

  private val thread = new Thread(() => run(), "Game")

  private lazy val strategy = {
    createBufferStrategy(3)
    getBufferStrategy
  }

  @volatile private var running = false

  private val screen = new Screen(WIDTH, HEIGHT)
  private val clock = new FixedStepClock
  private val level = Level.generate(64, 64)(Level.FixedTypeStrategy(1))
  private var scrollX, scrollY = 0

  setPreferredSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE))
  addKeyListener(Keyboard)

  /** @param args Command line arguments. */
  def main(args: Array[String]): Unit = {
    val frame = new JFrame("Rain")
    frame.add(this)
    frame.pack()
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
    frame.setLocationRelativeTo(null)
    frame.setResizable(true)
    frame.setVisible(true)
    requestFocus()
    start()
  }

  /** Starts the game. */
  def start(): Unit = {
    running = true
    thread.start()
  }

  /** Stops the game. */
  def stop(): Unit = {
    running = false
    System.exit(0)
  }

  /** Runs the main game loop. */
  def run(): Unit = {
    while (running) {
      clock.tick(
        update = Game.this.update,
        render = Game.this.render
      )
    }
  }

  /** Updates the game a single frame. */
  def update(): Unit = {
    import Keyboard.Key

    if (Keyboard.isKeyDown(Key.ESCAPE)) stop()
    if (Keyboard.isKeyDown(Key.W)) scrollY -= 1
    if (Keyboard.isKeyDown(Key.S)) scrollY += 1
    if (Keyboard.isKeyDown(Key.A)) scrollX -= 1
    if (Keyboard.isKeyDown(Key.D)) scrollX += 1
  }

  /** Renders the game a single frame. */
  def render(): Unit = {
    val graphics = strategy.getDrawGraphics

    screen.clear()
    level.render(scrollX, scrollY)(screen)
    screen.render(graphics, getWidth, getHeight)

    graphics.dispose()

    strategy.show()
  }
}