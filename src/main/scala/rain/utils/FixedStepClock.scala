/*
 * Copyright 2017, the project authors. All rights reserved.
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE.md file.
 */

package rain.utils

/** Represents a high-precision fixed-step clock for advancing the game state and
  * measuring frames per second and updates per second.
  *
  * @param ticksPerSecond The number of updates to execute every second.
  * @param logIntervalMs  How frequently to log FPS to the console, in milliseconds.
  */
class FixedStepClock(ticksPerSecond: Int = 60,
                     logIntervalMs: Int = 1000) {
  private val nsPerTick = 1e9 / ticksPerSecond
  private val nsPerMs = 1e6

  private var lastTimeNs = now()
  private var lastLogTimeNs = now()
  private var deltaTimeNs = 0.0

  private var frameCount = 0
  private var updateCount = 0

  /** Advances the clock a single frame.
    *
    * Updates and renders the game, via the given callbacks, as appropriate
    * to meet the required ticks per second.
    */
  def tick(update: () => Unit, render: () => Unit): Unit = {
    val currentTimeNs = now()
    deltaTimeNs += (currentTimeNs - lastTimeNs) / nsPerTick
    lastTimeNs = currentTimeNs

    // update a fixed number of times per second
    while (deltaTimeNs >= 1) {
      update()
      deltaTimeNs -= 1
      updateCount += 1
    }

    // render as much as possible
    render()
    frameCount += 1

    // log fps every n seconds
    val lastLogTimeMs = (currentTimeNs - lastLogTimeNs) / nsPerMs
    if (lastLogTimeMs > logIntervalMs) {
      println(s"FPS: $frameCount, UPS: $updateCount")
      lastLogTimeNs = currentTimeNs
      frameCount = 0
      updateCount = 0
    }
  }

  /** @return The current time, in nanoseconds. */
  private def now() = System.nanoTime()
}
