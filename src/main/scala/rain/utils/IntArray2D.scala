/*
 * Copyright 2017, the project authors. All rights reserved.
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE.md file.
 */

package rain.utils

/** Represents a 2d array of integers. */
class IntArray2D(array: Array[Int])(val width: Int, val height: Int) {
  /** Retrieves the value at the given (x, y). */
  def apply(x: Int, y: Int): Int = array(x + y * width)

  /** Updates the value at the given (x, y). */
  def update(x: Int, y: Int, value: Int): Unit = array(x + y * width) = value

  /** Fills the array with the given value. */
  def fill(value: Int): Unit = {
    for (y <- 0 until height) {
      for (x <- 0 until width) {
        array(x + y * width) = value
      }
    }
  }
}

object IntArray2D {
  /** Builds a new array of the given dimensions. */
  def apply(width: Int, height: Int)(initial: => Int) = new IntArray2D(Array.fill(width * height)(initial))(width, height)

  /** Wraps the given [[Array]] in an [[IntArray2D]].  */
  def wrap(source: Array[Int])(width: Int, height: Int) = new IntArray2D(source)(width, height)
}