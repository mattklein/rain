/*
 * Copyright 2017, the project authors. All rights reserved.
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE.md file.
 */

package rain.graphics

import java.awt.Graphics
import java.awt.image.{BufferedImage, DataBufferInt}

import rain.utils.IntArray2D

/** Represents a screen of pixels that may be rendered to an image.
  *
  * @param width  The width of the screen, in pixels.
  * @param height The height of the screen, in pixels.
  */
class Screen(val width: Int, val height: Int) {
  private val image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
  private val pixels = IntArray2D.wrap(image.getRaster.getDataBuffer.asInstanceOf[DataBufferInt].getData)(width, height)
  private var offsetX, offsetY = 0

  /** Clears the screen to black. */
  def clear(): Unit = {
    pixels.fill(0)
  }

  /** Sets the screen offset. */
  def setOffset(x: Int, y: Int): Unit = {
    offsetX = -x
    offsetY = -y
  }

  /** Draws the given [[Sprite]] at the given (x, y) screen-space coordinates. */
  def draw(sprite: Sprite)(offsetX: Int, offsetY: Int): Unit = {
    for (y <- 0 until sprite.height) {
      for (x <- 0 until sprite.width) {
        val xx = this.offsetX + offsetX + x
        val yy = this.offsetY + offsetY + y

        if (xx > 0 && xx < width &&
            yy > 0 && yy < height) {
          pixels(xx, yy) = sprite.sample(x, y)
        }
      }
    }
  }

  /** Renders the screen to the given [[Graphics]]. */
  def render(graphics: Graphics, width: Int, height: Int): Unit = {
    graphics.drawImage(image, 0, 0, width, height, null)
  }
}
