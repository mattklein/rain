/*
 * Copyright 2017, the project authors. All rights reserved.
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE.md file.
 */

package rain

package object graphics {
  /** Represents a packed RGB color value. */
  type Color = Int
}
