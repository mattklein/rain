/*
 * Copyright 2017, the project authors. All rights reserved.
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE.md file.
 */

package rain.graphics

/** Represents a sprite in some [[SpriteSheet]].
  *
  * @param width  The width of the sprite, in pixels.
  * @param height The height of the sprite, in pixels.
  */
abstract class Sprite(val width: Int, val height: Int) {
  /** Samples the sprite at the given (x, y) screen-space coordinates and returns it's RGB color. */
  def sample(x: Int, y: Int): Color
}

