/*
 * Copyright 2017, the project authors. All rights reserved.
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE.md file.
 */

package rain.graphics

import javax.imageio.ImageIO

import rain.utils.IntArray2D

/** Represents a sheet of sprite images.
  *
  * A sprite can be obtained from it's (x, y) offset into the image from the upper-left hand corner.
  *
  * Each sprite is expected to be the same size (uniform width and height).
  *
  * @param path The path to the sprite sheet image file.
  * @param size The size of each sprite, in pixels.
  */
class SpriteSheet(path: String, val size: Int) {
  private val image = ImageIO.read(getClass.getResource(path))
  private val pixels = IntArray2D.wrap(image.getRGB(0, 0, image.getWidth, image.getHeight, null, 0, image.getWidth))(image.getWidth, image.getHeight)

  /** Retrieves the [[Sprite]] at the given (x, y) offset in the sheet. */
  def get(offsetX: Int, offsetY: Int): Sprite = new Sprite(size, size) {
    override def sample(x: Int, y: Int): Color = {
      val xx = offsetX * size + x
      val yy = offsetY * size + y

      pixels(xx, yy)
    }
  }
}

/** The default/static [[SpriteSheet]]. */
object SpriteSheet extends SpriteSheet(path = "/textures/sprites.png", size = 16) {
  val grass: Sprite = get(0, 0)
  val water: Sprite = get(1, 0)
}

