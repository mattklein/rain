/*
 * Copyright 2017, the project authors. All rights reserved.
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE.md file.
 */

package rain.input

import java.awt.event.{KeyAdapter, KeyEvent}

/** Represents the keyboard device and buffers key-state changes. */
class Keyboard extends KeyAdapter {
  private val keys = Array.fill(65535)(false)

  def isKeyDown(key: Int): Boolean = keys(key)
  def isKeyUp(key: Int): Boolean = !keys(key)

  override def keyPressed(event: KeyEvent): Unit = {
    keys(event.getKeyCode) = true
  }

  override def keyReleased(event: KeyEvent): Unit = {
    keys(event.getKeyCode) = false
  }
}

/** The singleton [[Keyboard]] instance. */
object Keyboard extends Keyboard {
  /** Key codes for the keyboard. */
  object Key {
    import KeyEvent._

    val ESCAPE = VK_ESCAPE
    val W = VK_W
    val S = VK_S
    val A = VK_A
    val D = VK_D
  }
}