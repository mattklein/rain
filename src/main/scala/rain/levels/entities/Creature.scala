/*
 * Copyright 2017, the project authors. All rights reserved.
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE.md file.
 */

package rain.levels.entities

import rain.graphics.{Screen, Sprite}

/** Represents a creature in the game world. */
abstract class Creature extends Entity {
  /** (x, y) position of the creature. */
  val x, y: Int = 0

  /** The creature's [[Sprite]]. */
  val sprite: Sprite

  override def render()(implicit screen: Screen): Unit = {
    screen.draw(sprite)(x, y)
  }
}
