/*
 * Copyright 2017, the project authors. All rights reserved.
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE.md file.
 */

package rain.levels.entities

import rain.graphics.Screen

/** Represents an entity in the game */
abstract class Entity {
  def update(): Unit = {
    // no-op by default
  }

  def render()(implicit screen: Screen): Unit = {
    // no-op by default
  }
}
