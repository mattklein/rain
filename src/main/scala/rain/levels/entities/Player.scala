/*
 * Copyright 2017, the project authors. All rights reserved.
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE.md file.
 */

package rain.levels.entities

import rain.graphics.Sprite

/** The player [[Creature]]. */
class Player extends Creature {
  override val sprite: Sprite = null

  override def update(): Unit = {
  }
}
