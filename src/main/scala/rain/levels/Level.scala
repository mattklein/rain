/*
 * Copyright 2017, the project authors. All rights reserved.
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE.md file.
 */

package rain.levels

import rain.graphics.{Screen, SpriteSheet}
import rain.levels.entities.Entity
import rain.levels.tiles.{Tile, TileMap}

import scala.collection.mutable
import scala.util.Random

/** Represents a fixed-dimension level in the game.
  *
  * @param width  The width of the level, in tiles.
  * @param height The height of the level, in tiles.
  */
class Level(val width: Int, val height: Int) {
  private val map = new TileMap(width, height)
  private val entities = mutable.Set[Entity]()

  /** Updates the level. */
  def update(): Unit = {
    // update the game entities
    for (entity <- entities) {
      entity.update()
    }
  }

  /** Renders the level.  */
  def render(scrollX: Int, scrollY: Int)(implicit screen: Screen): Unit = {
    screen.setOffset(scrollX, scrollY)

    // calculate the render region
    val minX = scrollX / SpriteSheet.size - 1
    val minY = scrollY / SpriteSheet.size - 1
    val maxX = (scrollX + screen.width) / SpriteSheet.size + 1
    val maxY = (scrollY + screen.height) / SpriteSheet.size + 1

    // loop over the visible render region
    for (y <- minY until maxY) {
      for (x <- minX until maxX) {
        val tile = tileAt(x, y)
        tile.render(x, y)
      }
    }

    // render the game entities
    for (entity <- entities) {
      entity.render()
    }
  }

  /** Retrieves the [[Tile]] at the given (x, y). */
  def tileAt(x: Int, y: Int): Tile = map(x, y) match {
    case 0 => Tile.Water
    case 1 => Tile.Grass
    case _ => Tile.Void
  }
}

object Level {
  /** Generates a new [[Level]] with the given (width, height). */
  def generate(width: Int, height: Int)(strategy: GenerationStrategy = PureRandomStrategy(Random)): Level = {
    val level = new Level(width, height)
    strategy.generate(level)
    level
  }

  /** A strategy for generation [[Level]]s. */
  trait GenerationStrategy {
    /** Applies the strategy to the given [[Level]]. */
    def generate(level: Level): Unit

    /** Fills the level with tiles based on the given decider function. */
    protected def fillWithDecider(level: Level)(decider: (Int, Int) => Int): Unit = {
      for (y <- 0 until level.height) {
        for (x <- 0 until level.width) {
          level.map(x, y) = decider(x, y)
        }
      }
    }
  }

  /** A [[GenerationStrategy]] without rhyme or reason. */
  case class PureRandomStrategy(random: Random) extends GenerationStrategy {
    override def generate(level: Level) = fillWithDecider(level) { (x, y) =>
      random.nextInt(2)
    }
  }

  /** A [[GenerationStrategy]] that fills with a fixed tile type. */
  case class FixedTypeStrategy(tileId: Int) extends GenerationStrategy {
    override def generate(level: Level) = fillWithDecider(level) { (x, y) =>
      tileId
    }
  }
}
