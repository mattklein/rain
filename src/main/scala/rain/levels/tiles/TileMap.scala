/*
 * Copyright 2017, the project authors. All rights reserved.
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE.md file.
 */

package rain.levels.tiles

import rain.utils.IntArray2D

/** Represents a map of tiles.
  *
  * @param width         The width of the map, in tiles.
  * @param height        The height of the map, in tiles.
  * @param size          The size of each tile, in pixels.
  * @param defaultTileId The ID of the default tile to use for empty space and initial fill.
  */
class TileMap(val width: Int, val height: Int, size: Int = 16, defaultTileId: Int = 0) {
  private val tiles = IntArray2D(width, height)(defaultTileId)

  /** Samples the tile map at the given (x, y) screen-space coordinates, returning the tile id. */
  def apply(x: Int, y: Int): Int = {
    if (x < 0 || x >= width) return defaultTileId
    if (y < 0 || y >= height) return defaultTileId

    tiles(x, y)
  }

  /** Updates the tile id at the given (x, y) screen-space coordinates. */
  def update(x: Int, y: Int, tileId: Int): Unit = {
    if (x < 0 || x >= width) return
    if (y < 0 || y >= height) return

    tiles(x, y) = tileId
  }
}
