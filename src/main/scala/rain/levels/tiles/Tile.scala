/*
 * Copyright 2017, the project authors. All rights reserved.
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE.md file.
 */

package rain.levels.tiles

import rain.graphics.{Screen, Sprite, SpriteSheet}

/** Represents a tile for use in a [[TileMap]].
  *
  * This is a fly-weight over the different tile types that encapsulate common behaviour for all.
  */
sealed abstract class Tile(val sprite: Sprite) {
  val solid: Boolean = false

  /** Renders the tile to the given [[Screen]].  */
  def render(offsetX: Int, offsetY: Int)(implicit screen: Screen): Unit = {
    screen.draw(sprite)(offsetX * sprite.width, offsetY * sprite.height)
  }
}

/** The different tile implementations. */
object Tile {
  case object Void extends Tile(sprite = null) {
    override def render(offsetX: Int, offsetY: Int)(implicit screen: Screen): Unit = {
    }
  }
  case object Grass extends Tile(sprite = SpriteSheet.grass)
  case object Water extends Tile(sprite = SpriteSheet.water)
}
